// global data var with some pre-defined data
const data = new Array(
  {
    brandName: "audi-1",
    color: "white",
    customerName: "bikash",
    id: "1",
    regNo: "7379",
    vehicleName: "audi",
  },
  {
    brandName: "audi-1",
    color: "white",
    customerName: "bikash",
    id: "2",
    regNo: "7379",
    vehicleName: "audi",
  },
  {
    brandName: "BMW-2",
    color: "white",
    customerName: "bikash",
    id: "3",
    regNo: "7379",
    vehicleName: "BMW",
  }
);
let updateMode = false;
function displayData() {
  //  display table header in the table
  console.log(data);
  const tableData = document.getElementById("table-data");

  tableData.innerHTML = `
  <tr class="table-row">
    <th>ID</th>
    <th>Vehicle Name</th>
    <th>Brand</th>
    <th>Reg No</th>
    <th>Color</th>
    <th>Customer Name</th>
    <th></th>
</tr>`;

  data.map((val) => {
    tableData.innerHTML += `   <tr class="table-row">
                <td class="table-col">${val.id}</td>
                <td class="table-col">${val.vehicleName}</td>
                <td class="table-col">${val.brandName.split("-")[0]}</td>
                <td class="table-col">${val.regNo}</td>
                <td class="table-col">${val.color}</td>
                <td class="table-col">${val.customerName}</td>
                <td class="table-col">
                    <button class="update-btn" onclick="updateTable(${
                      val.id
                    })">Update</button>
                    <button class="dlt-btn" onclick="deleteData(${
                      val.id
                    })">Delete</button>
                </td>
            </tr>`;
  });

  //   RESET FORM VALUE
  document.getElementById("numId").value = "";
  document.getElementById("txtVehicleName").value = "";
  document.getElementById("sltBrandName").selectedIndex = "0";
  document.getElementById("txtRegNo").value = "";
  document.getElementById("txtColor").value = "";
  document.getElementById("txtCustomerName").value = "";
}
function checkId(id) {
  data.map((val) => {
    if (val.id == id) return true;
  });
}

function submitHandler(e) {
  e.preventDefault();
  const id = document.getElementById("numId").value;
  const vehicleName = document.getElementById("txtVehicleName").value;
  const brandName = document.getElementById("sltBrandName").value;
  const regNo = document.getElementById("txtRegNo").value;
  const color = document.getElementById("txtColor").value;
  const customerName = document.getElementById("txtCustomerName").value;
  if (!id || !vehicleName || !brandName || !regNo || !color || !customerName) {
    alert("all filed are required");
  } else {
    data.forEach((val, index) => {
      if (val.id == id) {
        console.log("present data");
        data[index] = { id, vehicleName, regNo, color, customerName, brandName };
        return displayData();
      }
    });

    // inserting new data
    if (updateMode == true) {
      updateMode = false; //back to previous state
    } else {
      data.push({ id, vehicleName, brandName, regNo, color, customerName });
      return displayData();
    }
  }
}

function deleteData(id) {
  data.forEach((val, index) => {
    if (val.id == id) {
      data.splice(index, 1);
    }
  });
  console.log(data);
  displayData();
}

function updateTable(id) {
  data.map((val) => {
    if (val.id == id) {
      // to get the brand name drop down index
      const sltIndex = val.brandName.split("-")[1];

      // populate value
      document.getElementById("numId").value = val.id;
      document.getElementById("txtVehicleName").value = val.vehicleName;
      document.getElementById("sltBrandName").selectedIndex = sltIndex;
      document.getElementById("txtRegNo").value = val.regNo;
      document.getElementById("txtColor").value = val.color;
      document.getElementById("txtCustomerName").value = val.customerName;
      updateMode = true;
      return;
    }
  });
}

(() => {
  displayData();
})();
