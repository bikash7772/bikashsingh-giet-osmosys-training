let numOfCol = window.prompt("number of column");
string = "";
if (numOfCol % 2 == 0) {
  window.alert("you entered even number");
} else {
  for (let row = 1; row <= parseInt(numOfCol); row++) {
    for (let col = 1; col <= parseInt(numOfCol) - row; col++) {
      string += " ";
    }
    for (let val = 1; val <= 2 * row - 1; val++) {
      string += val;
    }
    string += "\n";
  }
}
console.log(string);
