let input = window.prompt("enter comma-separated integer value");
input = input.split(",");
const arr = [];
input.map((val) => arr.push(parseInt(val)));
let maxCounter = 0;
let element = 0;
for (let i = 0; i < arr.length; i++) {
  let counter = 1;
  for (let j = i + 1; j < arr.length; j++) {
    if (arr[i] == arr[j]) {
      counter++;
    }
  }
  if (maxCounter < counter) {
    maxCounter = counter;
    element = arr[i];
  }
}

console.log(
  "The number " +
    element +
    " appears" +
    maxCounter +
    " more times than any other element in the array."
);
