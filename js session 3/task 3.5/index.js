const data = {
  TCS: {
    revenue: 1000000,
    expenses: {
      salaries: 30,
      rent: 20,
      utilites: 15,
    },
    employees: [
      {
        name: "Joe",
        age: 30,
        role: "Admin",
      },
      {
        name: "Jo",
        age: 40,
        role: "Tester",
      },
      {
        name: "Sherlock",
        age: 45,
        role: "Programmer",
      },
    ],
  },
  GGK: {
    revenue: 10000,
    expenses: {
      salaries: 30,
      rent: 20,
      utilites: 15,
    },
    employees: [
      {
        name: "Joe",
        age: 30,
        role: "Admin",
      },
      {
        name: "Sin",
        age: 40,
        role: "Tester",
      },
      {
        name: "Sherlock",
        age: 45,
        role: "Programmer",
      },
    ],
  },
  Osmosys: {
    revenue: 100000,
    expenses: {
      salaries: 30,
      rent: 20,
      utilites: 15,
    },
    employees: [
      {
        name: "Joe",
        age: 30,
        role: "Admin",
      },
      {
        name: "Bik",
        age: 40,
        role: "Tester",
      },
      {
        name: "Sherlock",
        age: 45,
        role: "Programmer",
      },
    ],
  },
};

class Split {
  lessThanAge(age) {
    let arr = [];
    let employee = [];
    for (let val in data) {
      arr.push(data[val]["employees"]);
    }
    arr.map((v) => {
      for (let i = 0; i < v.length; i++) {
        if (v[i]["age"] <= age) {
          employee.push(v[i]["name"]);
        }
      }
    });
    console.log(employee);
  }
  greatherThanAge(age) {
    let arr = [];
    let employee = [];
    for (let val in data) {
      arr.push(data[val]["employees"]);
    }
    arr.map((v) => {
      for (let i = 0; i < v.length; i++) {
        if (v[i]["age"] >= age) {
          employee.push(v[i]["name"]);
        }
      }
    });
    console.log(employee);
  }

  printRole() {
    let arr = [];
    let employee = [];
    for (let val in data) {
      arr.push(data[val]["employees"]);
    }
    arr.map((v) => {
      for (let i = 0; i < v.length; i++) {
        employee.push([v[i]["name"], v[i]["role"]]);
      }
    });
    console.log(employee);
  }

  printProfit() {
    let arr = [];
    let employee = [];
    for (let val in data) {
      arr.push([val, data[val]["revenue"], data[val]["expenses"]]);
    }
    arr.map((v) => {
      v.map((v1) => {});
    });
    console.log(arr);
  }
}

const a = new Split();
// a.lessThanAge(40);
// a.greatherThanAge(30);
// a.printRole();
a.printProfit();
