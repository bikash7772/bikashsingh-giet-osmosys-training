class Birthday {
  addBirthday(name, dayNumber, date) {
    console.log(typeof localStorage.getItem("data"));
    if (localStorage.getItem("data") == null) {
      let data = { birthdayList: [{ name, dayNumber, date }] };
      localStorage.setItem("data", JSON.stringify(data));
    } else {
      let data = JSON.parse(localStorage.getItem("data"));
      let { birthdayList } = data;
      birthdayList.push({ name, dayNumber, date });
      localStorage.setItem("data", JSON.stringify(data));
    }
  }
  birthdayOnThisMonth() {
    const date = new Date();
    let currentMonth = parseInt(date.toLocaleString().split("/")[1]);
    let birthdayList = [];
    const data = JSON.parse(localStorage.getItem("data"));
    data["birthdayList"].map(function (value) {
      if (parseInt(value["date"].split("/")[1]) == currentMonth) {
        birthdayList.push(value["name"]);
      }
    });
    console.log(birthdayList);
  }

  birthdayOnSundayThisYear() {
    let birthdayList = [];
    const data = JSON.parse(localStorage.getItem("data"));
    data["birthdayList"].map(function (value) {
      if (parseInt(value["dayNumber"]) == 7) {
        birthdayList.push(value["name"]);
      }
    });
    console.log(birthdayList);
  }
  upComingBirthday() {}
  oldestFriend() {}
}

const obj = new Birthday();
obj.addBirthday("bikash", 7, "10/06/2000");
obj.birthdayOnSundayThisYear();
obj.birthdayOnThisMonth();
